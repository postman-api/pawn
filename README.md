# PAWN #



### What is PAWN? ###

**PAWN** (**P**ostman **A**PI **W**orkspace **N**exus) is a group of public workspaces consisting of 
APIs, source & sample code, libraries, SDKs, documentation, and **Postman** components 
(**OpenAPI**, **Collections**, **Environments**, **Monitors**, and **Mock Servers**) dedicated to various subjects.